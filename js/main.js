"use strict";

// 1. У нас уже есть рабочее приложение, состоящее из отдельных функций. Представьте, что перед вами стоит задача переписать его так, чтобы все функции стали методами объекта personalMovieDB
// Такое случается в реальных продуктах при смене технологий или подхода к архитектуре программы
// 2. Создать метод toggleVisibleMyDB, который при вызове будет проверять свойство privat. Если оно false - он переключает его в true, если true - переключает в false. Протестировать вместе с showMyDB.
// 3. В методе writeYourGenres запретить пользователю нажать кнопку 'отмена' или оставлять пустую строку. Если он это сделал - возвращать его к этому же вопросу. После того, как все жанры введены - при помощи метода forEach вывести в консоль сообщения в таком виде: 'Любимый жанр #(номер по порядку, начиная с 1) - это (название из массива)'

//1

let personalMovieDB = {
  count: null,
  movies: {},
  actors: {},
  genres: [],
  private: false,
  // метод limit проверяет чтоб строка не была пустой, null или больше 50 символов
  // и отдает соответствующее булевое значение
  limit: function (string) {
    if (string == "" || string == null || string.length > 50) {
      return true;
    } else {
      return false;
    }
  },

  // метод kinoman проверяет count и вывод сообщение
  kinoman: function () {
    if (this.count < 10) {
      alert("Просмотрено довольно мало фильмов");
    } else if (this.count >= 10 && this.count <= 30) {
      alert("Вы классический зритель");
    } else if (this.count > 30) {
      alert("Вы киноман");
    } else {
      alert("Произошла ошибка");
    }
  },

  // метод numberOfFilms задает пользователю вопрос
  // проверяет введенную строку методом limit
  // если пользователь накосячил с вводом, то
  // рекурсивно запускает саму себя, то есть снова задает пользователю вопрос и снова проверяет его
  // когда пользователь наконец сделает корректный ввод, то вызовется функция kinoman, а далее произойдет выход из рекурсии
  numberOfFilms: function () {
    let temp = prompt("Сколько фильмов вы уже посмотрели?", "");
    if (this.limit(temp)) {
      this.numberOfFilms();
    } else {
      this.count = temp;
      this.kinoman();
    }
  },

  // проверяется каждый ввод, если он некорректный, то итерация сбрасывается
  // цикл будет повторяться до тех пор пока пользователь не сделает 2(на самом деле 4) корректный ввода
  filmRating: function () {
    for (let i = 0; i < 2; i++) {
      let filmName = prompt("Один из последних просмотренных фильмов?", "");
      if (this.limit(filmName)) {
        i--;
        continue;
      }
      let filmRating = prompt("На сколько оцените его?", "");
      if (this.limit(filmRating)) {
        i--;
        continue;
      }
      this.movies[filmName] = filmRating;
    }
  },

  // проверяем свойство private
  showMyDB: function () {
    if (!this.private) {
      console.log(personalMovieDB);
    }
  },

  // после каждого ввода запускаем проверку через limit,
  //если ввод проверку не прошел, то сбрасывает итерацию
  writeYourGenres: function () {
    for (let i = 0; i <= 2; i++) {
      let temp = prompt(`Ваш любимый жанр под номером ${i + 1}`);
      if (this.limit(temp)) {
        i--;
        continue;
      }
      this.genres[i] = temp;
    }
		this.genres.forEach(function(item, i){
			console.log(`Любимый жанр #${i+1} - это ${item}`)
		})
  },

  // 2
  toggleVisibleMyDB: function () {
    // я не понимаю зачем здесь нужна проверка, ведь мы в любом случае даем private противоположное значение
    this.private = !this.private;

		// реализация с проверкой, где мы явно указываем значение private
    // if(this.private){
    // 	this.private = false;
    // }else{
    // 	this.private = true;
    // }
  },
};

personalMovieDB.numberOfFilms();
console.log(personalMovieDB);
personalMovieDB.filmRating();
console.log(personalMovieDB);
personalMovieDB.writeYourGenres();
console.log(personalMovieDB);
